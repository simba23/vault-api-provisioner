##  Vault API based Provisioning

##  Usage

Assuming you have a Vault server and token to run this script. If you do not 
have Vault server, see [Start Up a Test Vault](#start-up-a-test-vault) below

    ls
    cat requirements.txt
    python3 -m venv provisioner-env
    source provisioner-env/bin/activate
    pip3 install -r requirements.txt
    
    export VAULT_ADDR=<ADDRESS TO VAULT>
    export VAULT_TOKEN=<TOKEN> 
    
    ./vault_api_provisioner.py -h
    ./vault_api_provisioner.py --log=Debug


## Adding Configuration

Place JSON formatted payloads, with the fields
and values required by Vault's API, in the
folder in this folder tree corresponding to
that API endpoint. 

Then add a line in the `vault_api_provisioner.py`
script to make it run against that folder, if you created a
new one. 

For example, you can put a new Vault Policy 
named `my_policy` in Vault HCL in the 
`data/sys/policy/` folder with a file name
of `my_policy.hcl`.

And if you want to enable 
new AWS authentication roles at an AWS 
authentication method you've enabled at 
the mount point `aws`, named `readonly`, 
you'd create a new folder and file for that at
`data/aws/roles/readonly.json`, 
following the related documentation 
for that API path:

https://www.vaultproject.io/api/secret/aws/index.html#create-update-role

Note that Sentinel and Vault policies are handled a little differently
than regular .json payloads, to account for allowing
comments and, in the case of Vault Policies, more
usable syntax checks.

##  Start Up a Test Vault

### Dev Mode Open Source Vault

If you want to test this out without modifying an existing Vault System,
the following will show some basic results.
These instructions work on Linux, and should also work on Mac if `wget` is installed.

            ##  Terminal 1 ##
    cd test
    wget 'https://releases.hashicorp.com/vault/1.1.2/vault_1.1.2_linux_amd64.zip'
    unzip vault_1.1.2_linux_amd64.zip
    ./vault server -dev -dev-root-token-id=root  ##  Now switch to a different terminal
            ##  Terminal 2 ##
    export VAULT_ADDR=http://127.0.0.1:8200
    export VAULT_TOKEN=root
    cd ../
    provision_vault/bin/vault_api_provisioner.sh

### To re-test

If running `vault server` in `-dev` mode, you can flush all of its data
simply by restarting it:

    killall vault  ##  Or ctrl+C in the terminal on which you'd run Vault
    ./vault server -dev -dev-root-token-id=root

### File Backend with Open Source Vault

    cd test
    wget 'https://releases.hashicorp.com/vault/1.1.2/vault_1.1.2_linux_amd64.zip'
    unzip vault_1.1.2_linux_amd64.zip
    ./vault server -config-file=test-vault.hcl
    cd ../
    export VAULT_ADDR=http://127.0.0.1:8200
    vi provision_vault/bin/vault_api_provisioner.sh ##  Look near the end of the file, and edit to run api_provisioner against the sys directory
    provision_vault/bin/vault_api_provisioner.sh
    export VAULT_TOKEN=<<Put the token response from the provisioner here>>
    provision_vault/bin/vault_api_provisioner.sh ##  Run it again now that you have a proper Vault token and an initialized Vault.

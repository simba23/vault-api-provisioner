#!/usr/bin/env python3

"""
Vault API Provisioner

Usage:
  vault_api_provisioner.py [-h | --help] [--version] [--log=INFO] [--init]
  vault_api_provisioner.py --init

Options:
  -h --help          Show this screen.
  --version          Show version.
  --log=<level>      Set log level, of ERROR, WARNING, INFO, DEBUG. Default is WARNING.
  --init             Initialize Vault


Linted with hatred by PyLint, from https://www.pylint.org

This script is designed to be run on a regular basis
as part of a minimal CI CD work flow for updating
Vault configuration. It can be combined with some
minimal tests, for instance to see if changes
break core functionality like authentication,
replication, and reading Secrets stored in Vault.
"""

# -------- Imports -------- #

import os  # For parsing directories and executing Vault CLI
import logging  # For creating a log file and allowing DEBUG logs.
import sys  # To allow sys.exit to leave the program
import json  # To parse JSON in Vault responses and payloads in the folder tree
import requests  # For communicating with Vault's HTTP API
from docopt import docopt  # Allows CLI flags like --log

VAULT_HTTP_CODE_SEALED: int = 503


def check_vault_status(_vault_addr="http://127.0.0.1:8200"):

    """Make sure Vault is reachable and not sealed.

    Will check localhost if no address supplied, and
    can handle a case in which Vault is unreachable.

    NOTE: This won't work with insecure TLS.
    :param _vault_addr:
    """
    LOGGER.debug("Checking Vault Status now.")
    try:
        response = requests.get(_vault_addr + "/v1/sys/health")
        LOGGER.debug("Server returned the following response status code in response to \
                     %s/v1/sys/health: ", VAULT_ADDR)
        LOGGER.debug(response.status_code)
        if not response.status_code:
            raise Exception('Server returned no response to '
                            + _vault_addr
                            + '. Do you have the right URI? Is it on?')
        if response.status_code == VAULT_HTTP_CODE_SEALED:
            raise Exception('The Vault at ' + _vault_addr
                            + ' returned an HTTP status code that indicates that it is sealed.'
                            + 'Please unseal the Vault or initialize it before continuing.')
        if response.status_code == 472:
            raise Exception('The Vault at ' + _vault_addr
                            + ' returned an HTTP status code that indicates '
                            + 'that it is an Enterprise DR Secondary.'
                            + 'Are you sure you want to auto-deploy configuration to it?')
        if response.status_code != 429 and response.status_code != 200 \
                and response.status_code != 501:
            raise ValueError("Something unexpected is going on with Vault, please \
                                troubleshoot further before continuing.")
    except Exception as _vault_error:
        LOGGER.warning("ERROR: ")
        LOGGER.warning(_vault_error)
        LOGGER.warning("I just tried to make a request to %s /v1/sys/health.", _vault_addr)
        LOGGER.warning("Vault was either unhealthy, sealed, in DR mode, not reachable, \
        or otherwise not in the expected state.")
        LOGGER.warning("Program will now exit.")
        sys.exit()


def is_initialized():
    """
    Check whether Vault is Initialized.

    This will check whether Vault has initialized,
    and log some of the data that it gets.
    """
    LOGGER.debug('Checking init')
    response = requests.get(VAULT_ADDR + "/v1/sys/health")
    LOGGER.debug(response.json()['initialized'])
    return response.json()['initialized'] == 'True'


def json_validate(data_to_validate):
    """Validate JSON, handle errors with it."""
    return_value = []
    try:
        json_loaded = "nothing_loaded"
        json_loaded = json.loads(data_to_validate)
        LOGGER.debug("Successfully loaded JSON: %s", json_loaded)
        return_value = [json_loaded, data_to_validate]
    except ValueError as json_error:
        LOGGER.error(json_error)
        return_value = [json_loaded, data_to_validate, json_error]
    finally:
        logging.debug(return_value)


def check_vault_env_vars():
    """Ensure Important Environment Variables exist

    Check for the presence of Vault
    environment variables. Note that this
    does not validate the values, it just
    checks whether they are present.
    For instance, even if someone set their
    VAULT_ADDR to some silly value like p3829gG*KEJOA,
    this would not throw an error."""
    if os.getenv("VAULT_ADDR") is None:
        print("Error: Environment variable VAULT_ADDR not set.")
        print("Please set VAULT_ADDR prior to running this script")
        sys.exit()

    elif os.getenv("VAULT_TOKEN") is None:
        print("Error: Environment variable VAULT_TOKEN not set.")
        print("Please set VAULT_TOKEN prior to running this script")
        sys.exit()
    else:

        return {"VAULT_ADDR": os.getenv("VAULT_ADDR"),
                "VAULT_TOKEN": os.getenv("VAULT_TOKEN")}


def provisioner(directory):
    """Provision Vault via direct curl API calls or Vault command line (CLI)

    Based on a given directory files ending in .json will call the
    api_provisioner_json() function, and .hcl files will call the
    cli_provisioner_policy() function

    Preconditions:
    VAULT_ADDR and VAULT_TOKEN
    must both be set.
    The directory from which you
    are deploying .json Vault Policies
    must contain either no .json
    files, or valid .json files.
    The directory you are targeting must exist.
    This overall script must be
    run from the root directory of this
    Git repository.
    The folder provision_vault/data
    must exist.

    Postconditions:
    Vault now has Vault configurations,
    each at the API endpoint that
    matches the name of the folder
    on the filesystem, with the filenames
    appended minus the ".json" part., and each
    with the contents of the .json files
    over which it iterated."""
    # The following is the equivalent of `cd "$CONFIG_DIRECTORY"`
    os.chdir(configuration_directory_absolute)

    for _json_file_name in os.listdir(directory):

        # Iterates over the files in the targeted directory.
        # NOTE: That is why I use globs here and in the future,
        # because iterating over ls output is fragile.
        #     https://github.com/koalaman/shellcheck/wiki/SC2045

        # IF we're debugging, check the Vault Status before each POST request to Vault:
        if LOGGER.level == logging.DEBUG:
            response_from_status = requests.get(VAULT_ADDR
                                                + "/v1/"
                                                + "sys/health")
            LOGGER.debug("response_from_status.text %s", str(response_from_status.text))
            LOGGER.debug("response_from_status.status_code: %s",
                         str(response_from_status.status_code))

        if _json_file_name.endswith(".hcl"):
            cli_provisioner_policy(directory, _json_file_name)
        elif _json_file_name.endswith(".json"):
            api_provisioner_json(directory, _json_file_name)

    logging.debug("The state in Vault is as follows:")
    response_from_get = requests.get(VAULT_ADDR + "/v1/" + directory, headers=VAULT_HEADERS)
    logging.debug("response_from_get.text %s", str(response_from_get.text))


def api_provisioner_json(directory, json_file_name):
    """Provision Vault with a given json file

    This takes a given json file in a directory,
    and POSTs it to Vault at an API path
    that matches the directory path plus
    the filename (minus that filename's extension).

    This will eat 400 errors with 'path is already in use'."""
    _json_file_name = json_file_name
    try:
        _json_file = open(directory + "/" + _json_file_name)

        _path_for_payload = directory + "/" + _json_file_name[:-5]  # Remove the .json
        #  file extension by removing the last five characters
        #  from file's relative path
        LOGGER.debug("json_file_name is %s", str(_json_file_name))
        LOGGER.debug("API Endpoint Path for Payload: %s", str(_path_for_payload))
        LOGGER.info("api_provisioner() is applying %s \
                    to the Vault cluster %s \
                    with the following API address: %s \
                    using %s as an API payload.",
                    str(_path_for_payload), str(VAULT_ADDR),
                    str(VAULT_ADDR + '/' + _path_for_payload),
                    str(_json_file_name))

        _payload_data = open(_path_for_payload + ".json", 'r').read()

        #  Say some information about what we're about to push to Vault:
        LOGGER.debug("_payload_data is: ")
        LOGGER.debug(_payload_data)
        LOGGER.debug("Request will go to: %s/v1/%s", VAULT_ADDR, _path_for_payload)
        LOGGER.debug("The value of the vault_headers variable is: %s", VAULT_HEADERS)

        response_from_post = requests.post(VAULT_ADDR + "/v1/"
                                           + _path_for_payload, headers=VAULT_HEADERS,
                                           data=_payload_data)

        if 'path is already in use' in response_from_post.text:
            logging.debug("json response that may show ignorable error is: %s",
                          str(response_from_post.json()))
            try:
                error_message = response_from_post.json()['errors'][0]
                if 'path is already in use' in error_message:
                    logging.debug(error_message)    # Don't raise an error
                    # for "path is already in use"
            except Exception as unexpected_non_200_status:
                #  Raise an exception if there is a non-200 response code
                logging.debug("Somehow, there was text labeled \
                        'path is already in use' in a response \
                        that was not of that type of error.")
                logging.debug(unexpected_non_200_status)
                response_from_post.raise_for_status()
        else:
            #  Raise an exception if there is a non-200 response code
            response_from_post.raise_for_status()

        LOGGER.debug("response_from_post.text %s", response_from_post.text)

    except Exception as api_call_exception:
        # Deal with exceptions here
        # ...
        LOGGER.error("ERROR in Vault API Call: %s", str(api_call_exception))
        LOGGER.info("Full api_call_exception is:")
        LOGGER.info(api_call_exception)


def cli_provisioner_policy(directory, file_name):
    """Provision Vault Policies

    Push .hcl files
    up to the Vault. Does this in a way
    that preserves content like
    comments and whitespace.

    Preconditions:
    VAULT_ADDR and VAULT_TOKEN
    must both be set.
    The directory from which you
    are deploying .hcl Vault Policies
    must contain either no .hcl
    files, or valid .hcl files.
    The directory you are targeting must exist.
    This overall script must be
    run from the root directory of this
    Git repository.
    The folder provision_vault/data
    must exist.

    Postconditions:
    Vault now has Vault policies, each
    with the contents of the .hcl files
    over which it iterated used
    as rules, and named after the
    filenames minus the ".hcl" part.
    """
    #  Deal with the file and its contents
    print(file_name)
    _hcl_file_name = file_name
    _hcl_file = open(directory + "/" + _hcl_file_name)

    _path_for_payload = directory + "/" + _hcl_file_name[:-4]  # Remove the .hcl
    #  file extension by removing the last five characters
    #  from file's relative path
    #  This is done to find the right API path for Vault.
    #  Not sure what happens when a file is named just .hcl.

    _payload_data = open(_path_for_payload + ".hcl", 'r').read()
    LOGGER.info("cli_provisioner_policies() is applying %s to the \
                Vault cluster with the following API address: \
                %s, using %s as an API payload.",
                _path_for_payload, VAULT_ADDR, file_name)
    LOGGER.info(".hcl file contents:")
    LOGGER.info(_payload_data)
    LOGGER.debug("Now runnning the following command on your system:")
    LOGGER.debug("vault write %s policy='%s'", _path_for_payload, _payload_data)
    os.system("vault write " + _path_for_payload + " policy='" + _payload_data + "'")
    LOGGER.debug("Reading the information back from Vault:")
    if LOGGER.level == logging.DEBUG:
        os.system("vault read " + _path_for_payload)  # For development Vault Clusters.
    #  This would not be used in production, where I recommend
    #  write-only provisioning, because it's more secure.


if __name__ == '__main__':  # Checks that this isn't being imported

    #  Get CLI arguments for Logging
    LOG_LEVEL = ''  # Declare log level
    ARGUMENTS = docopt(__doc__)
    # If the Log Level is set to DEBUG or INFO, print that to the console
    if isinstance(ARGUMENTS['--log'], str):
        if (ARGUMENTS['--log'].upper() == 'DEBUG') or (ARGUMENTS['--log'].upper() == 'INFO'):
            print("Log Level is: " + ARGUMENTS['--log'].upper())
        LOG_LEVEL = ARGUMENTS['--log'].upper()
    else:
        LOG_LEVEL = 'WARNING' # Default

    # -------- Logging Setup -------- #
    # Create logger
    LOGGER = logging.getLogger('provisioner')
    LOGGER.setLevel(LOG_LEVEL)
    # Create file handler which logs even debug messages
    FILE_LOGGER = logging.FileHandler('provisioner-debug.log')
    FILE_LOGGER.setLevel(logging.DEBUG)
    # Create console handler with a higher log level
    CONSOLE_LOGGER = logging.StreamHandler()
    CONSOLE_LOGGER.setLevel(LOG_LEVEL)
    # Create formatter and add it to the handlers
    LOG_FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    FILE_LOGGER.setFormatter(LOG_FORMATTER)
    CONSOLE_LOGGER.setFormatter(LOG_FORMATTER)
    # Add the handlers to the logger
    LOGGER.addHandler(FILE_LOGGER)
    LOGGER.addHandler(CONSOLE_LOGGER)
    LOGGER.info(os.path.basename(__file__))
    # -------- End Logging Setup ---- #

    #  Check Vault environment Variables
    check_vault_env_vars()

    #  Get and Show Information about how I'll use Vault
    VAULT_ADDR = os.environ['VAULT_ADDR']
    VAULT_TOKEN = os.environ['VAULT_TOKEN']
    VAULT_HEADERS = {'X-Vault-Token': VAULT_TOKEN}

    LOGGER.debug("VAULT_ADDR = %s", str(VAULT_ADDR))
    LOGGER.debug("Last 2 characters of the VAULT_TOKEN = %s", str(VAULT_TOKEN[-2:]))

    #  Check some things before running
    check_vault_status(VAULT_ADDR)

    #  Get and Show Current Location of .py File, and the location of our Data tree
    LOGGER.debug(os.path.dirname(os.path.abspath(__file__)))
    SCRIPT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
    LOGGER.debug("Current Location of this Script: %s", SCRIPT_DIRECTORY)
    CONFIGURATION_DIRECTORY_RELATIVE = ("./data")  # The data/ dir is
    # relative to the script location
    os.chdir(CONFIGURATION_DIRECTORY_RELATIVE)
    configuration_directory_absolute: str = os.getcwd()
    LOGGER.debug("Directory of Configuration Data for Vault: %s", configuration_directory_absolute)

    #  Show the folder tree of the Configuration Data for Vault
    if LOGGER.level == logging.INFO:
        LOGGER.debug('tree:')
        os.system('tree ' + configuration_directory_absolute)

    # -------- To Initialize Vault -------- #
    LOGGER.debug(ARGUMENTS)
    is_initialized()
    if ARGUMENTS['--init'] and not is_initialized():
        LOGGER.info("Provisioning init")
        provisioner("sys")
    else:
        LOGGER.info("Already initialized. Moving on...")

    # -------- System -------- #
    provisioner("sys/audit")
    provisioner("sys/auth")
#    provisioner("sys/namespaces")
    provisioner("sys/mounts")
#    provisioner("sys/mfa/method/okta")
    provisioner("sys/policy")

    # -------- Authentication Methods -------- #
#    provisioner("auth/ldap/groups")
#    provisioner("auth/ldap/users")
#    provisioner("auth/userpass/users")
    provisioner("auth/kubernetes")
    provisioner("auth/aadoidc")
    provisioner("auth/auth0oidc")

    # -------- Secrets Engines -------- #
    provisioner("kv")
    provisioner("kv/data")

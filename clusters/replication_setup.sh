#!/usr/bin/env bash

VAULT_ADDR_PRIMARY=$1
VAULT_ADDR_SECONDARY=$2
VAULT_TOKEN_PRIMARY=$3
VAULT_TOKEN_SECONDARY=$4

# Related docs:
# https://www.vaultproject.io/api/system/replication/
# https://www.vaultproject.io/docs/enterprise/replication/index.html
# https://learn.hashicorp.com/vault/operations/ops-disaster-recovery#important-note-about-automated-dr-failover

echo 'Replication Setup'

echo 'Given the Vault Cluster API addresses for two Vault clusters,
and a token for each, enable Vault Disaster Recovery (DR) Replication on the first
Cluster as a DR Primary, and on the second as its DR Secondary.'

echo 'Example usage:

      replication_setup.sh "http://127.0.0.1:8200" "http://127.0.0.1:7200" s.7p9K1zFkD2xrFicE  s.O8bmXgXjyMjEmLr
'

echo "Checking Vault Replication status on ${VAULT_TOKEN_PRIMARY} and ${VAULT_TOKEN_SECONDARY}"

curl \
    --insecure \
    --header "X-Vault-Token ${VAULT_TOKEN_PRIMARY}"\
    "${VAULT_ADDR_PRIMARY}/v1/sys/replication/status" | jq

curl \
    --insecure \
    --header "X-Vault-Token ${VAULT_TOKEN_SECONDARY}"\
    "${VAULT_ADDR_SECONDARY}/v1/sys/replication/status" | jq

echo 'Enable Vault Replication on the Primary'

curl --header "X-Vault-Token: ${VAULT_TOKEN_PRIMARY}" \
      --insecure \
      --request POST \
      --data '{}' \
      "${VAULT_ADDR_PRIMARY}/v1/sys/replication/dr/primary/enable" | jq

curl \
    --insecure \
    --header "X-Vault-Token ${VAULT_TOKEN_PRIMARY}"\
    "${VAULT_ADDR_PRIMARY}/v1/sys/replication/status" | jq

echo 'Generate a secondary token by invoking /sys/replication/dr/primary/secondary-token endpoint.'
curl --header "X-Vault-Token: ${VAULT_TOKEN_PRIMARY}" \
      --request POST \
      --data '{ "id": "secondary"}' \
      "${VAULT_ADDR_PRIMARY}/v1/sys/replication/dr/primary/secondary-token" > dr_secondary_wrapping_token.json

export DR_SECONDARY_WRAPPING_TOKEN=$(cat dr_secondary_wrapping_token.json | jq -r '.wrap_info.token')

echo "DR_SECONDARY_WRAPPING_TOKEN: $"

##  DR_SECONDARY_WRAPPING_TOKEN is the generated token which you will need to enable the DR secondary cluster.

echo 'Use the secondary token to enable DR Replication on the Vault cluster you want to be a DR Replication Secondary.'

curl --header "X-Vault-Token: ${VAULT_TOKEN_SECONDARY}" \
      --insecure \
      --request POST \
      --data "{\"token\": \"${DR_SECONDARY_WRAPPING_TOKEN}\"}" \
      ${VAULT_ADDR_SECONDARY}/v1/sys/replication/dr/secondary/enable | jq

curl \
    --insecure \
    --header "X-Vault-Token ${VAULT_TOKEN_PRIMARY}"\
    "${VAULT_ADDR_PRIMARY}/v1/sys/replication/status" | jq

sleep 3

curl \
    --insecure \
    --header "X-Vault-Token ${VAULT_TOKEN_SECONDARY}"\
    "${VAULT_ADDR_SECONDARY}/v1/sys/replication/status" | jq

printf "Congratulations, DR Replication is set up now.

Primary: ${VAULT_ADDR_PRIMARY}
            \ /
            | |
            \ \___________________________
             \_____DR_REPLICATION_______  \\
                                        | |
                                        \ /
                         Secondary: ${VAULT_ADDR_SECONDARY}
"
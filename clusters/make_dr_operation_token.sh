#!/usr/bin/env bash

# https://www.vaultproject.io/api/system/replication/replication-dr.html#promote-dr-secondary

# Usage: 

# ./get_dr_operation_token.sh <vault_addr_of_secondary_to_promote> <vault_token_of_secondary_to_promote> <unseal_keys_comma_separated>

# Usage examples: 

# ./get_dr_operation_token.sh 'http://127.0.0.1:8202' root 'TVOp75iBx4SRICJ+LuSOvRjn34RPZOEv95dscMHTqow=,LIlsL3PSMfwd57QAmiG3NkPfGGNC0Dr5cwU8V35Fhug='
# ./get_dr_operation_token.sh $VAULT_ADDR $VAULT_TOKEN $(cat keys.csv)

# NOTE: The unseal key provided should NOT be the unseal key you got when you initialized the Secondary
# Vault Cluster. It should be the Unseal key from the Primary cluster.

# Set an internal value for a delimiter for strings, according to this doc:
# https://www.tutorialkart.com/bash-shell-scripting/bash-split-string/
IFS=','

VAULT_ADDR_SECONDARY=$1
VAULT_TOKEN=$2
UNSEAL_KEY_CSV=$3
read -ra ARR <<< "${UNSEAL_KEY_CSV}"  # Uses $IFS value applied above
                # You may now access the tokens split into an array
		# using a bash for loop. Use it pirate-style, with $ARR.
UNSEAL_KEY_ARRAY=$ARR
for KEY in "${UNSEAL_KEY_ARRAY[@]}"
do
    echo $KEY
done

# Get a DR Operation Token
curl \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt

# Start DR Operation Token Generation
# https://www.vaultproject.io/api/system/replication/replication-dr.html#start-token-generation

curl \
    --request PUT \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt > promote.json

# Provide Key Share for Token Generation
# https://www.vaultproject.io/api/system/replication/replication-dr.html#provide-key-share-to-generate-token

cat promote.json | jq
cat promote.json | jq -r '.nonce'
cat promote.json | jq -r '.otp'

NONCE=$(cat promote.json | jq -r '.nonce')
OTP=$(cat promote.json | jq -r '.otp')
echo "Nonce is ${NONCE}"
echo "UNSEAL_KEY_ARRAY=${UNSEAL_KEY_ARRAY}"
echo 'Getting Encoded DR Operation Token'
for KEY in "${UNSEAL_KEY_ARRAY[@]}"
do
    echo $KEY
    curl \
        --request PUT \
        --data "{\"key\": \"${UNSEAL_KEY_ARRAY}\", \"nonce\": \"${NONCE}\"}" \
        $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/update > dr_operation_token.json
done

cat dr_operation_token.json | jq

DR_OPERATION_TOKEN_ENCODED=$(cat dr_operation_token.json | jq -r '.encoded_token')

echo "DR_OPERATION_TOKEN_ENCODED=${DR_OPERATION_TOKEN_ENCODED}"

expr length "${DR_OPERATION_TOKEN_ENCODED}"

echo "./vault operator generate-root -address=\"${VAULT_ADDR_SECONDARY}\" -dr-token \
        -decode=\"${DR_OPERATION_TOKEN_ENCODED}\" \
        -otp=\"${OTP}\""

echo 'Decoding DR Operation Token'
DR_OPERATION_TOKEN=$(./vault operator generate-root -address="${VAULT_ADDR_SECONDARY}" -dr-token \
        -decode="${DR_OPERATION_TOKEN_ENCODED}" \
	-otp="${OTP}")

echo "DR_OPERATION_TOKEN=${DR_OPERATION_TOKEN}"


# Cancel operation in case it fails and we need to re-try with a new Nonce
# https://www.vaultproject.io/api/system/replication/replication-dr.html#cancel-generation

echo 'Cancelling'
curl \
    --request DELETE \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt

#!/usr/bin/env bash

# https://www.vaultproject.io/api/system/replication/replication-dr.html#promote-dr-secondary

# Usage: 

# ./promote.sh <vault_addr_of_secondary_to_promote> <vault_token_of_secondary_to_promote> <unseal_key1>

# Usage example: 

# ./promote.sh 'http://127.0.0.1:8202' root 'TVOp75iBx4SRICJ+LuSOvRjn34RPZOEv95dscMHTqow='

# NOTE: The unseal key provided should NOT be the unseal key you got when you initialized the Secondary
# Vault Cluster. It should be the Unseal key from the Primary cluster.

VAULT_ADDR_SECONDARY=$1
VAULT_TOKEN=$2
UNSEAL_KEY1=$3

# Get a DR Operation Token
curl \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt

# Start DR Operation Token Generation
# https://www.vaultproject.io/api/system/replication/replication-dr.html#start-token-generation

curl \
    --request PUT \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt > promote.json

# Provide Key Share for Token Generation
# https://www.vaultproject.io/api/system/replication/replication-dr.html#provide-key-share-to-generate-token

cat promote.json | jq
cat promote.json | jq -r '.nonce'
cat promote.json | jq -r '.otp'

NONCE=$(cat promote.json | jq -r '.nonce')
OTP=$(cat promote.json | jq -r '.otp')
echo "Nonce is ${NONCE}"
echo "UNSEAL_KEY1=${UNSEAL_KEY1}"
echo 'Getting Encoded DR Operation Token'
curl \
    --request PUT \
    --data "{\"key\": \"${UNSEAL_KEY1}\", \"nonce\": \"${NONCE}\"}" \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/update > dr_operation_token.json

cat dr_operation_token.json | jq

DR_OPERATION_TOKEN_ENCODED=$(cat dr_operation_token.json | jq -r '.encoded_token')

echo "DR_OPERATION_TOKEN_ENCODED=${DR_OPERATION_TOKEN_ENCODED}"

expr length "${DR_OPERATION_TOKEN_ENCODED}"

echo "./vault operator generate-root -address=\"${VAULT_ADDR_SECONDARY}\" -dr-token \
        -decode=\"${DR_OPERATION_TOKEN_ENCODED}\" \
        -otp=\"${OTP}\""

echo 'Decoding DR Operation Token'
DR_OPERATION_TOKEN=$(./vault operator generate-root -address="${VAULT_ADDR_SECONDARY}" -dr-token \
        -decode="${DR_OPERATION_TOKEN_ENCODED}" \
	-otp="${OTP}")

echo "DR_OPERATION_TOKEN=${DR_OPERATION_TOKEN}"

echo 'Trying promote'
curl \
    --header "X-Vault-Token: ${DR_OPERATION_TOKEN}" \
    --request POST \
    --data "{\"dr_operation_token\": \"${DR_OPERATION_TOKEN}\"}" \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/promote


# Cancel operation in case it fails and we need to re-try with a new Nonce
# https://www.vaultproject.io/api/system/replication/replication-dr.html#cancel-generation

echo 'Cancelling'
curl \
    --request DELETE \
    $VAULT_ADDR_SECONDARY/v1/sys/replication/dr/secondary/generate-operation-token/attempt

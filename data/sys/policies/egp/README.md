The .json files in this directory are not
actually in JSON format.

They're base64 encoded for direct use
with the Vault API, following this
example: 

    FILE_NAME=cidr-check.sentinel
    base64 cidr-check.sentinel > cidr-check.sentinel.json

The reason they're named .json is for backwards
compatibility with the API provisioning scripts
in this repository, which iterate over files
ending with .json. 

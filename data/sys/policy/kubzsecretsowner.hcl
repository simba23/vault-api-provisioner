path "kv/data/consumers/*" {
  capabilities = ["create", "delete"]
}

# For the owner of a secret, they can create these only, and rotate by
# deletion.
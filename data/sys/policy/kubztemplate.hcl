path "kv/data/consumers/{{identity.entity.aliases.auth_kubernetes_b33dc88a.metadata.service_account_name}}/*" {
  capabilities = ["list", "read"]  ##  Read only
}

path "kv/data/consumers/{{identity.entity.aliases.auth_kubernetes_b33dc88a.metadata.service_account_name}}/writeable/*" {
  capabilities = ["create"]  ##  Create only, for review by admins if needed
}

# For actual usage, the auth_kubernetes_b33dc88a hash 
# will get replaced by whatever accessor you have
# in your list of auth methods you
# get when you run vault auth list. 

# To set this up for use by one service, say, epaytrack, 
# you would run the following: 
# vault write auth/kubernetes/role/epaytrack \
#    bound_service_account_names=epaytrack \
#    bound_service_account_namespaces=default \
#    policies=kubztemplate \
#    ttl=10h
#  vault kv put kv/consumers/epaytrack lennypassphrase='٩(▀̿Ĺ̯▀̿ ̿٩)三(°_ʖ°)'
